
const int analogInPin = A0;  // Analog input pin that the potentiometer is attached to
const int analogSeuil = A1;
const int ledOut = 3;
const int relaisOut = 2;
bool relaisOn = false;

int sensorValue = 0;        // value read from the pot

int score =0;
int scoreAdd = 600;
int scoreSeuil = 650;
int seuil = 30;


unsigned long relaisMinDuration = 3000;
unsigned long relaisMaxDuration = 60000;
unsigned long lastRelaisStart = 0;
unsigned long lastRelaisContinue = 0;
int  waitTheEnd = 5000;


void setup() {
  // initialize serial communications at 9600 bps:
  Serial.begin(9600);
  pinMode (relaisOut, OUTPUT);
  pinMode(ledOut, OUTPUT);

  digitalWrite(relaisOut, HIGH);
}

void loop() {
  // read the analog in value:
  sensorValue = analogRead(analogInPin);
  seuil = analogRead(analogSeuil);
  
  if(sensorValue>seuil) {
    score += scoreAdd;
    digitalWrite(ledOut,HIGH);
  } else {
    digitalWrite(ledOut,LOW);
  }

  score = 2*score/3;


  Serial.print("seuil : ");Serial.print(seuil);Serial.print(" ; analogRead : ");
  Serial.print(sensorValue);Serial.print(" ; score : ");Serial.print(score); Serial.print(" ; relaisOn : ");Serial.print(relaisOn);Serial.print(" lastRelaisStart+relaisMaxDuration<millis() : ");Serial.println(lastRelaisStart+relaisMaxDuration<millis());

  if( score>scoreSeuil&& !relaisOn){
    relaisOn = true;
    digitalWrite(relaisOut, LOW);
    lastRelaisStart = millis();
  } else if( score<scoreSeuil && relaisOn && lastRelaisContinue+relaisMinDuration<millis() ){
    relaisOn = false;
    digitalWrite(relaisOut, HIGH);
  } else if( relaisOn && lastRelaisStart+relaisMaxDuration<millis()) {
    digitalWrite(relaisOut, HIGH);
    relaisOn = false;
    delay(waitTheEnd);
  }
  
  if(relaisOn && score>scoreSeuil) {
    lastRelaisContinue = millis();
  }


 

  // wait 2 milliseconds before the next loop for the analog-to-digital
  // converter to settle after the last reading:
  delay(2);
}
