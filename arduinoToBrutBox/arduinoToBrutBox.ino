#include <elapsedMillis.h>

#include <USB-MIDI.h>

USBMIDI_CREATE_DEFAULT_INSTANCE();

unsigned long t1 = millis();



 
// the MIDI channel number to send messages
const byte channel = 0;
 
// the MIDI continuous controller for each analog input
const byte controllerA0 = 0; //
const byte controllerA1 = 1; //
const byte controllerA2 = 2; // 
const byte controllerA3 = 3; //
const byte controllerA4 = 4; // 
const byte controllerA5 = 5; // 
const byte controllerA6 = 6; // 
const byte controllerA7 = 7; // 
 
 
// store previously sent values, to detect changes
int previousA0 = -1;
int previousA1 = -1;
int previousA2 = -1;
int previousA3 = -1;
int previousA4 = -1;
int previousA5 = -1;
int previousA6 = -1;
int previousA7 = -1;
 
elapsedMillis msec = 0;

void setup()
{
  Serial.begin(9600);
  MIDI.begin();
}

 
void loop() {
  Serial.print(analogRead(A0));Serial.print(" ");Serial.print(analogRead(A1));Serial.print(" ");Serial.print(analogRead(A2));Serial.print(" ");Serial.print(analogRead(A3));Serial.print(" ");Serial.println(analogRead(A6));
  // only check the analog inputs 50 times per second,
  // to prevent a flood of MIDI messages
  if (msec >= 50) {
    msec = 0;
    int n0 = analogRead(A0) / 8;
    int n1 = analogRead(A1) / 8;
    int n2 = analogRead(A2) / 8;
    int n3 = analogRead(A3) / 8;
    int n4 = analogRead(A4) / 8;
    int n5 = analogRead(A5) / 8;    
    int n6 = analogRead(A6) / 8;
    int n7 = analogRead(A7) / 8;    
    
 
    // only transmit MIDI messages if analog input changed
    if (n0 != previousA0) {
      controlChange(controllerA0, n0, channel);
      previousA0 = n0;
    }
    if (n1 != previousA1) {
      controlChange(controllerA1, n1, channel);
      previousA1 = n1;
    }
    if (n2 != previousA2) {
      controlChange(controllerA2, n2, channel);
      previousA2 = n2;
    }
    if (n3 != previousA3) {
      controlChange(controllerA3, n3, channel);
      previousA3 = n3;
    }
    if (n4 != previousA4) {
      controlChange(controllerA4, n4, channel);
      previousA4 = n4;
    }
    if (n5 != previousA5) {
      controlChange(controllerA5, n5, channel);
      previousA5 = n5;
    }
 
    if (n6 != previousA6) {
      controlChange(controllerA6, n6, channel);
      previousA6 = n6;
    }
    if (n7 != previousA7) {
      controlChange(controllerA7, n7, channel);
      previousA7 = n7;
    }
 
 
  }


  MIDI.read();
 
}

// First parameter is the event type (top 4 bits of the command byte).
  // Second parameter is command byte combined with the channel.
  // Third parameter is the first data byte
  // Fourth parameter second data byte

void controlChange(byte control, int value,byte channel) {
  midiEventPacket_t event = {0x0B, 0xB0 | channel, control, (byte)value};
  MidiUSB.sendMIDI(event);
  MidiUSB.flush();
}
