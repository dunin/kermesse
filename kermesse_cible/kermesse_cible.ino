/*
 * Code cible kermesse shooting
 */

#define Luminosite1 A3
#define Luminosite2 A4
#define Luminosite3 A5

#define Relais1 5
#define Relais2 6
#define Relais3 7

 #define CalibrationBtn 12


 const int nbCibles = 3; // CHANGER AUSSI les deux lignes suivantes et les define (ou mettre directement les valeurs des pins dans les tableaux ({A0,A1,...})
 int capteurs[nbCibles] = {Luminosite1,Luminosite2,Luminosite3};
 int relais[nbCibles] = {Relais1,Relais2,Relais3};
 int seuilDif = 80;
 int shakeDuration = 2000;
 int seuilScore = 300;
 int seuilAdd = 600;
 
 int score[nbCibles];
 int normalLum[nbCibles];
 unsigned long lastShoot[nbCibles];
 bool shooting[nbCibles];



// Fonction setup(), appelée au démarrage de la carte Arduino
void setup() {

  // Initialise la communication avec le PC
  Serial.begin(9600);

  //pinMode (CalibrationBtn, INPUT_PULLUP);
  for (int i=0;i<nbCibles;i++) {
    pinMode (relais[i], OUTPUT);
    digitalWrite(relais[i], true);
    score[i] = 0;
    lastShoot[i] = 0;
    shooting[i] = false;
  }
  
  startInit();
}

// Fonction loop(), appelée continuellement en boucle tant que la carte Arduino est alimentée
void loop() {
    int dummy;
    
    for (int i=0;i<nbCibles;i++) {
      if( !digitalRead(relais[i]) && lastShoot[i]+shakeDuration<millis() ) {
        digitalWrite(relais[i], true);
        
        score[i] = 0;
      } else if (shooting[i] && millis()>lastShoot[i]+shakeDuration) {
        shooting[i] = false;
        score[i] = 0;
      normalLum[i] = analogRead(capteurs[i]);
    } else if (!shooting[i]) {
      
      //if(!digitalRead(relais[i])) digitalWrite(relais[i], true);
      dummy = analogRead(capteurs[i]);
      int lum = analogRead(capteurs[i]);
      
      if(lum>normalLum[i]+seuilDif) {
          score[i] += seuilAdd;
          
      }
      normalLum[i] = (normalLum[i]+lum)/2;

      score[i] = (2*score[i]) /3;

      Serial.print(i); Serial.print(" : "); Serial.print(score[i]); Serial.print(" ; ");Serial.print(" lum : ");Serial.print(lum);

      if (score[i]>seuilScore){
        Serial.print("BANG ");
        Serial.print(i+1);
        Serial.println(" !");
        digitalWrite(relais[i], false);
        lastShoot[i] = millis();
        shooting[i] = true;
      }

      //delay(10);
    }
  }
  //Serial.print(analogRead(capteurs[0]));
  Serial.println("");
}


void startInit() {
  Serial.println("Init");
  for (int i=0;i<nbCibles;i++) {
      normalLum[i] = 0;
      digitalWrite(relais[i], true);
      shooting[i] = false;
      lastShoot[i] = 0;

      int lum = analogRead(capteurs[i]);
      normalLum[i] = lum;
    }
}
