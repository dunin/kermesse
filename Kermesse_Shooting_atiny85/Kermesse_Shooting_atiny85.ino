//LASER
#define Laser 3

//CLAP
#define Clap 4

bool armed = true;
bool laserOn = false;
int laserDuration = 50;//30;
int rearmedDuration = 50;
int countdown;
long startTime;
int delayStart = 500;

void setup() {
  pinMode (Laser, OUTPUT);
  pinMode (Clap, INPUT_PULLUP);
  //delay(1000);
  //Serial.begin(115200);
  startTime = millis();
  digitalWrite(Laser, HIGH);

}

void loop() {  
  if(millis()>startTime + delayStart) {
    
  
    if (laserOn) {
      digitalWrite(Laser, HIGH);
      laserOn = false;
    }
  
    if (armed) {
      if (digitalRead(Clap)) {
        armed = false;
        digitalWrite(Laser, LOW);
        laserOn = true;
        countdown = 0;
        delay(laserDuration);
      }
      
    } else {
      if(!digitalRead(Clap)) {
        countdown++;
      } else {
        countdown = 0;
      }
      if(countdown>rearmedDuration) {
        armed = true;
      }
    }
      
  }
  /*Serial.print("Armed: ");
  Serial.print(armed);
    Serial.print(" ; Countdown: ");
  Serial.print(countdown);
  Serial.print(" ; Laser: ");
  Serial.println(laserOn);*/
}
