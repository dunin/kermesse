#include <MozziGuts.h>
#include <Oscil.h>
#include <tables/cos8192_int8.h> // table for Oscils to play
#include <mozzi_midi.h> // for mtof
#include <mozzi_fixmath.h>

#define POTPIN A7

#define CONTROL_RATE 128 // Hz, powers of 2 are most reliable
#define ROWS 8
#define COLS 8

Oscil<COS8192_NUM_CELLS, AUDIO_RATE> aCos1(COS8192_DATA);
Oscil<COS8192_NUM_CELLS, AUDIO_RATE> aCos2(COS8192_DATA);
Oscil<COS8192_NUM_CELLS, AUDIO_RATE> aCos3(COS8192_DATA);
Oscil<COS8192_NUM_CELLS, AUDIO_RATE> aCos4(COS8192_DATA);
Oscil<COS8192_NUM_CELLS, AUDIO_RATE> aCos5(COS8192_DATA);
Oscil<COS8192_NUM_CELLS, AUDIO_RATE> aCos6(COS8192_DATA);
Oscil<COS8192_NUM_CELLS, AUDIO_RATE> aCos7(COS8192_DATA);
Oscil<COS8192_NUM_CELLS, AUDIO_RATE> aCos8(COS8192_DATA);


byte volume = 255;
//byte colScan = 0;

byte rowPins[ROWS] = {2, 3, 4, 5, 6, 7, 8, 10}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {11, 12, 14, 15, 16, 17, 18, 19}; //connect to the column pinouts of the keypad

//byte colsVal[COLS*2] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; // 8 vals for the 8 cols, then 8 another that are bits inverse
byte colsVal[COLS] = {0,0,0,0,0,0,0,0};
short lastVal = 0;

void setup(){
  for (byte r=0; r<ROWS; r++) {
    pinMode(rowPins[r],INPUT_PULLUP);
    pinMode(colPins[r],OUTPUT);
  }
  
  startMozzi(CONTROL_RATE);
  // set harmonic frequencies
  aCos1.setFreq(mtof(60));
  aCos2.setFreq(mtof(74));
  aCos3.setFreq(mtof(64));
  aCos4.setFreq(mtof(77));
  aCos5.setFreq(mtof(67));
  aCos6.setFreq(mtof(81));
  aCos7.setFreq(mtof(60));
  aCos8.setFreq(mtof(84));

  Serial.begin(9600);
}

String msg="";
void updateControl(){
  if(volume>0) {
    volume--;
  }
  //volume = mozziAnalogRead(POTPIN)/4;
  for (byte c=0; c<COLS; c++) {
    scanKey(c);
  }

  short newVal = colsVal[0] + colsVal[1] + colsVal[2] + colsVal[3] + colsVal[4] + colsVal[5] + colsVal[6] + colsVal[7];

  if(lastVal != newVal) {
    lastVal = newVal;
    volume = 200;
  }
  //colScan = (colScan+1)&7;
  
  //debugBoard();
}


AudioOutput_t updateAudio(){
    long asig = (long)
    aCos1.next()*colsVal[0] +
    aCos2.next()*colsVal[1] +
    aCos3.next()*colsVal[2] +
    aCos4.next()*colsVal[3] +
    aCos5.next()*colsVal[4] +
    aCos6.next()*colsVal[5] +
    aCos7.next()*colsVal[6] +
    aCos8.next()*colsVal[7];
    asig >>= 8;// shift back into range
    return (((int) asig)*volume)>>8; 
}

void debugBoard(){
  Serial.print("\033[s");
          Serial.println("--------");
         for(int j=0;j<COLS;j++){
            byte v = colsVal[j];

            // 2124 program / 182 RAM:-
            for (int k = 0; k < 8; k++)
            {
                bool b = v & 0x80;
                Serial.print(b);
                v = v << 1;
            }
            Serial.print(" | ");
            v = colsVal[j+8];
            for (int k = 0; k < 8; k++)
            {
                bool b = v & 0x80;
                Serial.print(b);
                v = v << 1;
            }
            Serial.println("");
         }
        Serial.print("--------");
        Serial.print("\033[u");
}

void scanKey(byte col) {
  // bitMap stores ALL the keys that are being pressed.
    
    digitalWrite(colPins[col], LOW); // Begin column pulse output.
    for (byte r=0; r<ROWS; r++) {
      
      if(!digitalRead(rowPins[r]) ) {
        colsVal[col] |= 1<<r;
        //colsVal[col+8] |= 0x80>>r;

      } else {
        colsVal[col] &= ~(1<<r);
        //colsVal[col+8] &= ~(0x80>>r);
      }
    }
    // Set pin to high impedance input. Effectively ends column pulse.
    digitalWrite(colPins[col],HIGH);

}

void loop(){
  audioHook();
}
