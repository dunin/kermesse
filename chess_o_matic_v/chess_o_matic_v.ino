#include <MozziGuts.h>
#include <Oscil.h>
#include "PDResonant.h"
// wavetable for oscillator:
#include <tables/sin2048_int8.h>
#include <IntMap.h>
#include <EventDelay.h>

#define POTPIN A7

#define CONTROL_RATE 128 // Hz, powers of 2 are most reliable
#define ROWS 8
#define COLS 8

PDResonant voice;
EventDelay endNote;
const IntMap kmapX(0, 255, 0, 1000); // Attack
const IntMap kmapY(0, 255, 0, 2000); //Decay


byte volume = 255;
//byte colScan = 0;

byte rowPins[ROWS] = {2, 3, 4, 5, 6, 7, 8, 10}; //connect to the row pinouts of the keypad
byte colPins[COLS] = {11, 12, 14, 15, 16, 17, 18, 19}; //connect to the column pinouts of the keypad

//byte colsVal[COLS*2] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}; // 8 vals for the 8 cols, then 8 another that are bits inverse
byte colsVal[COLS] = {0,0,0,0,0,0,0,0};
short lastVal = 0;

byte currentNote = 0;

void setup(){
  for (byte r=0; r<ROWS; r++) {
    pinMode(rowPins[r],INPUT_PULLUP);
    pinMode(colPins[r],OUTPUT);
  }
  
  startMozzi(CONTROL_RATE);
  // set harmonic frequencies

  Serial.begin(9600);
}

void noteOn(byte pitch,byte att, byte dec){
  voice.noteOn(1, pitch, 127);
  unsigned int attack = kmapX(att);
  unsigned int decay = kmapY(dec);
  voice.setPDEnv(attack,decay);
}


void noteOff(byte pitch){
  voice.noteOff(1, pitch, 0);
}


void updateControl(){
  //volume = mozziAnalogRead(POTPIN)/4;
  for (byte c=0; c<COLS; c++) {
    scanKey(c);
  }

  if(endNote.ready() && currentNote!=0){
    noteOff(currentNote);
    currentNote = 0;
  }

  voice.update();
  
  //debugBoard();
}


AudioOutput_t updateAudio(){
    return voice.next();
}

void debugBoard(){
  Serial.print("\033[s");
          Serial.println("--------");
         for(int j=0;j<COLS;j++){
            byte v = colsVal[j];

            // 2124 program / 182 RAM:-
            for (int k = 0; k < 8; k++)
            {
                bool b = v & 0x80;
                Serial.print(b);
                v = v << 1;
            }
            Serial.print(" | ");
            v = colsVal[j+8];
            for (int k = 0; k < 8; k++)
            {
                bool b = v & 0x80;
                Serial.print(b);
                v = v << 1;
            }
            Serial.println("");
         }
        Serial.print("--------");
        Serial.print("\033[u");
}

void scanKey(byte col) {
  // bitMap stores ALL the keys that are being pressed.
    
    digitalWrite(colPins[col], LOW); // Begin column pulse output.
    for (byte r=0; r<ROWS; r++) {
      byte lastVal = colsVal[col];
      
      if(!digitalRead(rowPins[r]) ) {
        colsVal[col] |= 1<<r;
        //colsVal[col+8] |= 0x80>>r;

      } else {
        colsVal[col] &= ~(1<<r);
        //colsVal[col+8] &= ~(0x80>>r);
      }

      if(lastVal!=colsVal[col]){
        launchNote(col, lastVal>colsVal[col] );
      }
    }
    // Set pin to high impedance input. Effectively ends column pulse.
    digitalWrite(colPins[col],HIGH);

}

void launchNote(byte col,bool petit){
  if(currentNote!=0){
    noteOff(currentNote);
  }

  if(petit) {
    currentNote = 40 + ( (colPins[col]&0x7C) >>2 ); // on prend 0XXXXX00 
  } else {
    currentNote = 30 + ( (colPins[col]&0x3E) >>1 ); // on prend 00XXXXX0
  }

  Serial.print("currentNote : ");Serial.println(currentNote);
  
  
  noteOn(currentNote, colPins[col], (colPins[col]&3) | (colPins[col]>>6)<<4  );

  if(petit) {
    endNote.set(500);
  } else {
    endNote.set(1000);
  }
  
  endNote.start();
  
}

void loop(){
  audioHook();
}
