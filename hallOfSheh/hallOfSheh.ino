const int nbBtn = 5;

int btnPins[nbBtn] = {2,3,4,5,6};   
int ledPins[nbBtn] = {8,9,10,11,12};   
const int relaisPin =  7; 

int currentGood = 0;
unsigned long changeLed = 2000;
unsigned long lastChange = 0;

unsigned long bzzTime = 3000;
unsigned long startBzzTime = millis();

bool play = true;

void setup() {
  Serial.begin(115200);
  
  for(int i=0;i<nbBtn;i++) {
    pinMode(ledPins[i], OUTPUT);
    pinMode(btnPins[i], INPUT_PULLUP);
  }

  pinMode(relaisPin, OUTPUT);
  digitalWrite(relaisPin, HIGH);

}

void loop() {
  if(play) {
    //chenillard
    if(lastChange+changeLed<millis()){
      digitalWrite(ledPins[currentGood],LOW);
      currentGood = (currentGood+1)%nbBtn;
      digitalWrite(ledPins[currentGood],HIGH);
      lastChange = millis();
    }

    // test boutons
    for(int i=0;i<nbBtn;i++) {
      Serial.print("i : ");Serial.print(digitalRead(btnPins[i]));Serial.print(" ; ");
      if(!digitalRead(btnPins[i])) {
        if(i!= currentGood) {
          digitalWrite(relaisPin, LOW);
          startBzzTime = millis();
        }
        play=false;
      }
      

      
    }
    Serial.println(" ");
  } else {
    if(startBzzTime+bzzTime < millis() ) {
      digitalWrite(relaisPin, HIGH);
      bool btnOff = true;
      for(int i=0;i<nbBtn;i++){
        if(!digitalRead(btnPins[i])){
          btnOff = false;
        }
      }
  
      if(btnOff) {
        play = true;
      }
    }

    
  }

}
