var amp;
let mic;
var niveau = 0;
var lastMax = 0;
var countdown = 0;
var scoreBoard;
var textSz; 
var offsetX;

function preload(){
  scoreBoard = loadFont('DangerOnTheMotorway-xV10.ttf');
}

function setup() {
  let cnv = createCanvas(windowWidth, windowHeight);
 
  textAlign(RIGHT);
  mic = new p5.AudioIn();
  amp = new p5.Amplitude();
  mic.start();
  amp.setInput(mic);

  fill(0,255,0);
  textFont(scoreBoard);
  textSz = height-20;
  textSize(textSz);
  while(textWidth("000")>width) {
    textSz--;
    textSize(textSz);
  }

  keyPressed();

  frameRate(5);

  window.addEventListener("resize",setup);
}

function draw() {

  background(0);

  var micLevel = amp.getLevel(0);
  // console.log(micLevel);
  if(micLevel > lastMax) {
    countdown = 10;
    lastMax = micLevel;
  } else {
    countdown--;
    if(countdown>0) micLevel = lastMax;
    else lastMax = micLevel;
  }

  var txtLevel = ""+parseInt(micLevel*999);
  while(txtLevel.length<3) txtLevel = "0"+txtLevel;
  text(txtLevel, windowWidth+textSz/11-offsetX, height-(height-textSz)/2);
}

function keyPressed(){
  if(key == "ArrowUp") textSz +=8;
  else if(key == "ArrowDown") textSz -=8;

  textSize(textSz);
  offsetX = (width-textWidth("000")+textSz/11)/2;
}